# Load Flask
import flask
app = flask.Flask('test')

#Run the example as follows
# http://127.0.0.1:5000/post?msg=name
#Where name = any name you want


# Set up a function that will allow us to utilise as a the web-serving
@app.route("/post", methods=["GET", "POST"])

# Process the request
def post():
	data = {"Who is the best tutor?":False}
	params = flask.request.json
	print(params)
	if (params == None):
		params = flask.request.args
	print(params)
	if (params != None):
		data["response"] = params.get("msg")
		data["response"] = data["response"].lower()
		if data["response"] == 'benny' or data["response"] == 'ben':
			data["Who is the best tutor"] = True
			response = 'You have gotten the correct answer. \n'
		else:
			response = 'You have obviously have not thought this answer properly. \n'
	return flask.jsonify(response + str(data))

app.run(host='0.0.0.0')
