# Transfer Learning Challenge 

## Instructions:

Create your own CNN transfer learning network to determine between Pizza and Not Pizza. 

You are to research and use a new pre trained model other than Mobilenet and train your model to reach the highest accuracy. 

You must also gather your own training and testing datasets for pizza and not pizza.

Run your code as a python script. Make sure you push everything to git so we can see your progress with the challenge. 

Have fun and goodluck!


